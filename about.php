<?php 
error_reporting(E_ERROR | E_PARSE);

//force https instead of http
echo '<script>if (location.protocol != \'https:\'){
 location.href = \'https:\' + window.location.href.substring(window.location.protocol.length);
}</script>';

$pageTitle = "About | Framework";
$pageHeader = "About";

include("includes/header.php");
?>

<div class="row">

	<textarea class="form-control" id ="text_area" rows="20">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus in lacus tincidunt, mattis massa a, tempor tellus. Proin viverra condimentum velit, a ultricies massa mattis in. Sed ipsum risus, gravida in dapibus in, ornare in neque. Etiam gravida nisi leo, sit amet laoreet velit lobortis quis. Nam nec consectetur ligula, in blandit magna. Donec commodo euismod suscipit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut ultricies purus nec enim bibendum sagittis. Etiam varius ex tortor, vel iaculis nulla volutpat in. Phasellus enim magna, cursus at rhoncus sed, mattis vel eros. Ut lobortis quam sed rutrum commodo. </textarea>
	<br />
</div>