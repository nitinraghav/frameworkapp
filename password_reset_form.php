<?php 
error_reporting(E_ERROR | E_PARSE);
	$CSS = "style.css";
	$pageTitle = "Forgot Password | Framework";
	$pageHeader = "Forgot Password";
	include("includes/header.php");
?>
<?php
session_start();
$_SESSION['token']= $_GET['token'];
?>

	<form action="password_reset.php" method="post">
   		<label id="forgot_password_form_text"> Enter your new password:</label>
   		<input type="password" name="password" placeholder="password" class="form-control" id="reset_form_password" /> <p id="response1"></p>

   		<label id="forgot_password_form_text2"> Re-Enter your new password:</label>
   		<input type="password" name="re_password" placeholder="Re-enter password" class="form-control" id="reset_form_re_password" /> <p id="response2"></p>

    	<input type="submit" value="Change Password"  class="btn btn-block btn-primary" id="forgot_password_form_button" />
	</form>

<script type="text/javascript">
	$(document).ready(function () {

		//client-side password restrictions atleast 6 characters long
			$("#reset_form_password").on("keyup", function(){

				if($("#reset_form_password").val().length < 6){
					$("#response1").html("Password should be atleast 6 characters long");
					
				} else if($("#reset_form_password").val().length >= 6){
					$("#response1").html("");
				}
			});//end password length

		// client-side check if both passwords match
		    $("#reset_form_re_password").on("keyup", function () {	
		        var matched,
		            password = $("#reset_form_password").val(),
		            confirm = $("#reset_form_re_password").val();
		        matched = (password === confirm) ? true : false;

		        if(matched) { 
		            //Shows success message and prevents submission.  In production, comment out the next 2 lines.
		            $("#response2").html("Passwords Match !");
		            $("#response2").css("color", "green");
			    }
			    else { 
			        $("#response2").html("Passwords don't match..");    
			        $("#response2").css("color", "red");
			    }
			});//end password match
		});//end ready
</script>
</body>
</html>

