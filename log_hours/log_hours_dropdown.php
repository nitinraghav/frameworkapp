<?php
session_start();

//check if user is logged in
if(!isset($_SESSION['user_id'])){
	header("location: ../login.php");
} else {
	//caching the user id from set cookie
	$user_id= $_SESSION['user_id'];

	//connect to DB
	include("../includes/dbc.php");
	$artwork= [];
	$artwork[0]= [];
	$artwork[1]= [];

	$query= "SELECT artwork_name FROM artwork WHERE winner_user_id= '".$user_id."'";
	$result= mysqli_query($conn, $query);

	while($row= mysqli_fetch_assoc($result)){
		$artwork[0][]= $row['artwork_name'];
	}

	$query2= "SELECT organization_name FROM organizations";
	$result2= mysqli_query($conn, $query2);
	while($row2= mysqli_fetch_assoc($result2)){
		$artwork[1][]= $row2['organization_name'];
	}
	echo json_encode($artwork);
}