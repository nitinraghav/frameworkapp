<?php
session_start();
//check if user is logged in
if(!isset($_SESSION['user_id'])){
	header("location: https://framework.launchliveapp.com/webapp/login.php");
} else {
	//caching the user id from set cookie
	$user_id= $_SESSION['user_id'];
}
?>
<head>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!--timepicker CSS-->
  <link rel="stylesheet" type="text/css" href="timepicker/jquery.timepicker.css">
</head>

<body>

	<div class="container">

		<h1>Volunteer Log</h1>

		<form action="log_hours/log_hours_backend.php" method="POST" id="log_hour_form">

			<!--Date picker-->
			<div class="row">	
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">	
					<label id="start-hour-log"> Date: </label>
				</div>

				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<input type="text" name="start-date" class="form-control datepicker" placeholder="Date" id= "start-date" required="required" /> 
				</div>
			</div>

			<!--Start time picker-->
			<div class="row">	
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">	
					<label id="start-hour-log"> Start Time: </label>
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<input type="text" name="start-time" class="form-control timepicker" placeholder="Time" id="start-time" required="required" />
				</div>
			</div>

			<!--End time picker-->
			<div class="row">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<label id="end-hour-log"> End Time: </label>
				</div>

				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<input type="text" name="end-time" class="form-control timepicker" placeholder="Time" id="end-time" required="required" /> 
				</div>
			</div>	

			<div class="row">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<label id="artwork-label"> Artwork: </label>
				</div>
				
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">

					<select type="text" class="form-control" placeholder="Artwork" name="artwork-dropdown" id="artwork-dropdown"> 
						<option class="dropdown_value" value="" disabled selected>Select your Artwork</option> 	
					</select>

				</div>
			</div>

			<div class="row">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<label id="organization-label"> Volunteer Organization: </label>
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<select type="text" class="form-control" placeholder="Organization" name="organization-dropdown" id="organization-dropdown" required="required" >
						<option class="dropdown_value" value="" disabled selected>Select your Organization</option> 
					</select>
				</div>
			</div>

			<textarea class="form-control" name="notes" id ="hour_log_notes" rows="5" placeholder="Notes.." id="log-hour-notes"></textarea>

			<button type="submit" class="btn btn-primary" id="log-hour-btn">Add to Log</button>

		</form>
	</div>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

</body>
</html>