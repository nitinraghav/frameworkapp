<?php
//error_reporting(E_ALL);
session_start();
//check if user is logged in
if(!isset($_SESSION['user_id'])){
	header("location: https://framework.launchliveapp.com/webapp/login.php");

} else {
	//caching the user id from set cookie
	$user_id= $_SESSION['user_id'];

	//connect to DB
	include("../includes/dbc.php");

	// getting user entered values from log_hours.php
	$start_date= $_POST['start-date'];
	
	$start_time= $_POST['start-time'];
	//$end_date= $_POST['end-date'];
	$end_time= $_POST['end-time'];

	$artwork= $_POST['artwork-dropdown'];
	$organization= $_POST['organization-dropdown'];
	$notes= $_POST['notes'];

	//error handling for empty required fields
	if(empty($start_date)) {
		header("location: log_hours.php");
		exit;
	}

	if(empty($start_time)) {
		header("location: log_hours.php");
		exit;
	}

	if(empty($end_time)) {
		header("location: log_hours.php");
		exit;
	}

	if(empty($organization)) {
		header("location: log_hours.php");
		exit;
	}

	//declaring time difference function
	function timeDiff($start_time, $end_time){
		$start_time= strtotime($start_time);
		$end_time= strtotime($end_time);

		if($start_time > $end_time){
			$end_time += 1200;
		}

		$time_diff= (($end_time- $start_time)/60)/60;
		return $time_diff;
	};

	$total_time = timeDiff($start_time, $end_time);

	//check if artwork is entered by user
	if(!empty($artwork)) {
		//check if email id exists in DB(user table)
		$query= "SELECT * FROM artwork WHERE artwork_name= '".$artwork."' AND winner_user_id= '".$user_id."' ";
		$result = mysqli_query($conn, $query);

		if($row= mysqli_fetch_assoc($result)){

			$art_id= $row['art_id'];

			$query2= "INSERT INTO `user_hour_log`(`user_id`, `date_time_of_entry`, `log_start_date`, `log_start_time`, `log_end_time`, `total_time`, `art_id`, `artwork_name`, `notes`) VALUES ('".$user_id."', NOW(),'".$start_date."','".$start_time."','".$end_time."','".$total_time."','".$art_id."','".$artwork."', '".$notes."')";

			$result2 = mysqli_query($conn, $query2);

		 	if($result2){
		 		echo "updated"; 
		 	} else {
		 		echo "dberror";
		 	}
		}

	 //if user wants to log hours without artwork assigned 
	} else if(empty($artwork)) {
		$query3= "INSERT INTO `user_hour_log`(`user_id`, `date_time_of_entry`, `log_start_date`, `log_start_time`, `log_end_time`, `total_time`, `notes`) VALUES ('".$user_id."', NOW(),'".$start_date."','".$start_time."','".$end_time."','".$total_time."','".$notes."')";

		$result3 = mysqli_query($conn, $query3);

	 	if($result3){
	 		echo "updated"; 
	 	} else {
	 		echo "dberror";
	 	}
	}

}