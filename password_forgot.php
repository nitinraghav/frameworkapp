<?php
error_reporting(E_ALL);
$email=$_POST['email'];

if(!$email){
	echo "emailnotentered";
	exit;

} 

if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
	echo 'invalidemail';
	exit;
}

include("includes/dbc.php");

$q= "SELECT email FROM user WHERE email='".$email."'";		
$r= mysqli_query($conn, $q);
$n= mysqli_num_rows($r);

if($n==0){
	echo "emailnotavailable";
	exit();
	
} else {

	//build a random string function
	function getRandomString($length) {
	    $validCharacters = "ABCDEFGHIJKLMNPQRSTUXYVWZ123456789";
	    $validCharNumber = strlen($validCharacters);
	    $result = "";

	    for ($i = 0; $i < $length; $i++) {
	        $index = mt_rand(0, $validCharNumber - 1);
	        $result .= $validCharacters[$index];
	    }
		return $result;
	}
	
	$token= getRandomString(10);

	//check if user has an used password token in DB
	$query3= "SELECT * FROM pswd_reset_token WHERE email='".$email."' && used=0";
	$result3= mysqli_query($conn, $query3);
	$num_rows= mysqli_num_rows($result3);

	if($num_rows==0){
		$sql= "INSERT INTO pswd_reset_token (token,email) VALUES ('".$token."','".$email."')";
		$result= mysqli_query($conn, $sql);
	}else {
		$query2= "UPDATE pswd_reset_token SET token='".$token."' WHERE email='".$email."'";
        $result2= mysqli_query($conn, $query2);
	}

	$to= $email;
	$subject = "Password reset link for Framework App";
	$uri = 'http://framework.launchliveapp.com/webapp/';
	$message = '
	<html>
	<head>
	<title>Forgot Password For Framework App</title>
	</head>
	<body>
	<p>Click on the given link to reset your password <a href="'.$uri.'password_reset_form.php?token='.$token.'">Reset Password</a></p>

	</body>
	</html>
	';
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
	$headers .= 'From: Admin<nitin@getliveapp.com>' . "\r\n";
	$headers .= 'Cc: nitin@getliveapp.com' . "\r\n";

	if(mail($to,$subject,$message,$headers)){
		echo "We have sent the password reset link to your email id: ".$to.""; 
	}
}

	
	