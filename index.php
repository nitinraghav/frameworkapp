<?php
error_reporting(E_ERROR | E_PARSE);

//force https instead of http
echo '<script>if (location.protocol != \'https:\'){
 location.href = \'https:\' + window.location.href.substring(window.location.protocol.length);
}</script>';

session_start();
$pageTitle = "Home | Framework";
$pageHeader = "Home";
include("includes/header.php");

//check if user is logged in
if( !isset($_SESSION['facebook_access_token']) && !isset($_SESSION['user_logged_in'])){
	echo '<script>alert("You are not logged in")</script>';
	echo "<script>window.location='https://framework.launchliveapp.com/webapp/login.php'</script>";
	
} else {
	//caching the user id 
	$user_id= $_SESSION['user_id'];

	//connect to DB
	include("includes/dbc.php");

	//query DB for total hours for a user
	$query= "SELECT *, SUM(highest_bid_hours) AS highest_bid_hours FROM artwork WHERE winner_user_id = '".$user_id."'";
	$result = mysqli_query($conn, $query);
	$num_rows= mysqli_num_rows($result);
	$row =mysqli_fetch_assoc($result);
	$total_hours = $row['highest_bid_hours'];

	//query DB for hours completed by a user
	$query2= "SELECT *, SUM(total_time) AS total_time FROM user_hour_log WHERE user_id = '".$user_id."'";
	$result2 = mysqli_query($conn, $query2);
	$num_rows2= mysqli_num_rows($result2);
	$row2 = $result2 -> fetch_assoc();
	$hours_completed = $row2['total_time'];
}
?>

<div id="wrapper">

	<!--3d circular graph on home page-->
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
		google.charts.load("current", {packages:["corechart"]});
		google.charts.setOnLoadCallback(drawChart);
		function drawChart() {
		    var data = google.visualization.arrayToDataTable([
		      	['Task', 'Hours Completed'],
		      	['Completed',     <?php echo $hours_completed; ?>],
		      	['Remaining',     <?php echo $total_hours-$hours_completed ;?>],
		    ]);

		    var options = {
		      	pieHole: 0.75,
		      	is3D: true,
		      	legend:'bottom',
		      	'width':340,
	          	'height':300,
	          	'chartArea': {'left':-5, 'width': '100%', 'height': '80%'},
	          	colors: ['#27CCC0', '#d5dae2']
		    };

		    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
		    chart.draw(data, options);
		}
	</script>
	  
	<div id="donutchart" style="width: 340px; height: 300px; display: block; margin: 0 auto;"></div>
	<!--3d circular graph ends-->
	
	<div class="row">
		<div class="col-md-12" >
			<span id="hours_completed" >
				<?php
					if($total_hours == 0){
				?>      <script> $("#circ_graph_holder").html("<br/><br/><br/><br/><br />");</script>
				<?php
					} else {

						$hours_completed= number_format($hours_completed,0);
						echo $hours_completed." of ". $total_hours. " volunteer hours logged";

						$x = $hours_completed;
						$y = $total_hours;
						$percent= number_format($x/$y * 100 , 0);
						echo "<br />".$percent. " % of Goal achieved";
					}
				?>
			</span>					
		</div>
	</div>

	<div class="col-md-12" >
		<button class="btn btn-primary all-buttons" id ="artwork_btn">My Artwork</button>
	</div>

	<div class="col-md-12" >
		<button class="btn btn-primary all-buttons" id ="bid">Bid On Art</button>
	</div>
	<div class="col-md-12" >
		<button class="btn btn-primary all-buttons" id ="log_hours">Log Hours</button>
	</div>
	<div class="col-md-12" >
		<button class="btn btn-primary all-buttons" id ="settings">Settings</button>
	</div>
</div><!--end wrapper-->
</div><!--end container starting in header.php-->
		

<!--my artwork button-->
<script>
	$(document).ready(function() {
		//my artwork button
		$("#artwork_btn").on("click", function(){

			$.ajax("artwork/artwork.php", {
				type: "POST",
				success: function(data){
					if(data == "noartwork"){
						alert("You have no Artwork Assigned \nPlease Bid on new Artwork");  
					} else {
						$("#wrapper").html(data);
					}
				}
			});//end ajax
		});//end click

		//display prize details
		$("#wrapper").on("click",".prize_img", function(e){
			e.preventDefault();

			$.post(this.href, function(data){
				if(data=="noimage"){
					alert("Sorry no Image available for this Artwork");
				} else {
					$("#wrapper").html(data);
				}

			});//end post
		});//end on click
	}); //end ready 
</script>

<!--bid on art button-->
<script>
	$(document).ready(function(){
		$("#bid").on("click", function(){
			$("#wrapper").html('<form action="bid/get_bid.php" method="POST" id ="search"><input class="form-control" name="artwork_id" type="text" placeholder= "Enter Artwork Code" id="artwork_search" required="required" /><button type="submit" class="btn btn-block btn-primary" name="submit" id="check_bid">Check Bid </button></form><div id="response1"></div>');
		});// end bid click

		//using 'on' event handler on #wrapper as it doesnt change in previous '.get' call and listening to submit event of the form
		$("#wrapper").on("submit","#search", function(e){
			e.preventDefault();

			var url = $(this).attr("action");
			var formData = $(this).serialize();

			$.ajax(url, {
				data : formData,
				type: "POST",
				success: function(data){
					$("#response1").html(data);
					$("#artwork_search").val("");

				} // end success
			}) // end ajax
		});//end on

		//updating new bid
		$("#wrapper").on("submit","#bid", function(evt){
			evt.preventDefault();
			var url2 = $(this).attr("action");
			var formData2 = $(this).serialize();

			$.ajax(url2, {
				data : formData2,
				type: "POST",
				success: function(data2){
					
					switch(data2){
						case("bidupdated"):
						alert ("Bid Updated Successfully !");
						$("#current_bid").val($("#new_bid").val());
						var next_bid= parseInt($("#new_bid").val()) + parseInt(10);
						$("#new_bid").val(next_bid);
						break;

						case("errorupdating"):
						alert ("Error updating bid\nPlease try again");
						break;

						case("invalidbid"):
						alert ("Invalid Bid \nNew Bid should be atleast 10hours more than existing bid and a maximum of 150hours\nPlease try again");
						break;

						default:
						window.location.replace("index.php");
						break;
					}//end switch
				} // end success

			}) // end ajax
		}); //end submit   

	});//end ready
</script>

<!--log hours button-->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="timepicker/jquery.timepicker.min.js"></script>

<script>
	$(document).ready(function(){
		$("#log_hours").on("click", function(){
			$.ajax("log_hours/log_hours.php",{
				async: true,
				success: function(data){
					$("#wrapper").html(data);
					$('.datepicker').datepicker();
					$('.timepicker').timepicker({ 'step': 30 });
					$('.timepicker').timepicker({ 'forceRoundTime': true });
				},
				complete: function(){
					$.get("log_hours/log_hours_dropdown.php", function(data){
						var artwork= JSON.parse(data);

						for(i=0; i < artwork[0].length; i++){
							$("#artwork-dropdown").append('<option class="dropdown_value" value="'+artwork[0][i]+'">'+artwork[0][i]+'</option>');
						};

						for(j=0; j < artwork[1].length; j++){
							$("#organization-dropdown").append('<option class="dropdown_value" value="'+artwork[1][j]+'">'+artwork[1][j]+'</option>');
						};


					});
				},
			});//end ajax
		});//end click

		$('#wrapper').on("submit","#log_hour_form", function(e) {
          	e.preventDefault();

			var formData = $(this).serialize();

			$.ajax("log_hours/log_hours_backend.php", {
				async: true,
				data: formData,
				type:"POST",
				success: function(response){
					
					if(response== "updated"){
						
						window.location.replace("https://framework.launchliveapp.com/webapp/index.php");
						alert("Log Hours updated successfully!");
					} else if (response=="dberror"){
						alert("Connection error \n Please try again");
					} 
				}
			});//end ajax
		});//end submit
	});//end ready
</script>

<!--settings button-->
<script>
	$(document).ready(function(){
		$("#settings").on("click", function(){
			$("#wrapper").html('<form action="settings/settings.php" method="POST" id ="settings_form"><input class="form-control" name="team_code" type="text" placeholder= "Enter Team Code" id="team_code" required="required" /><button type="submit" class="btn btn-block btn-primary" name="submit" id="settings_btn">Submit </button></form>');

		});//end click

		$("#wrapper").on("submit", "#settings_form", function(e){
			e.preventDefault();

			var url= $(this).attr("action");
			var formData= $(this).serialize();

			$.post(url, formData, function(data){
				if(data== "updated"){
					alert ("Team Code updated successfully!");
				} else if (data=="error"){
					alert ("Sorry, Team Code not found \n Please try again");
				}

			});//end post
		});//end submit
	});//end ready
</script>
</body>
</html>