<?php
error_reporting(E_ALL);
session_start();
$token= $_SESSION['token'];

$pass= $_POST['password'];
$re_password = $_POST['re_password'];

if(empty($pass) || strlen($pass) < 6) {
    echo"Password should be atleast 6 characters long";
    exit;
}

if(!($re_password == $pass)) {
    echo"Both passwords should match";
    exit;
}

include("includes/dbc.php");

if(isset($pass)){
    $email='';

    $q="SELECT * FROM pswd_reset_token where token='".$token."' && used=0";
    $r=mysqli_query($conn, $q);
    
    if($r){

        $row= mysqli_fetch_assoc($r);
        $email= $row['email'];
        $_SESSION['email']= $email;

    } else die("Invalid link or Password already changed");

    if(isset($pass) && isset($email)) {
        // hash password before storing it in database
        $hash_pswd = password_hash($pass, PASSWORD_DEFAULT);

        $query="UPDATE user SET password='".$hash_pswd."' WHERE email='".$email."'";
        $result= mysqli_query($conn, $query);

        if($result){
            $query2= "UPDATE pswd_reset_token SET used=1 WHERE token='".$token."'";
            $result2= mysqli_query($conn, $query2);

            if($result2){
                echo "Your password is changed successfully";
            } else{
                echo "An error occurred while changing password. Please try again.";
            }
        } else {
            die("An error occurred while changing password. Please try again.");
        }
    } else{ 
        die("An error occurred while changing password. Please try again.");
    }
}
?>