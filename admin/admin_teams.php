<?php

$pageTitle = "Teams";
$CSS= "admin_style.css";
include("../includes/admin_header.php");

include("../includes/dbc.php");

$query= "SELECT * FROM teams";
$result= mysqli_query($conn, $query);
$num= mysqli_num_rows($result);

if($num===0){
	echo "<p>There are no Teams to display</p>";
} else {

	echo "<div class=\"container\">
			<table class=\"desktop-table\">
				<thead>
					<tr>
						<th>Team Name</th>
						<th>Team Code</th>
						<th>City</th>
						<th>Province</th>
						<th>Contact Name</th>
						<th>Email</th>
						<th>Phone Number</th>
					</tr>
				</thead>
				<tbody>";

	// output data of each row
	while($row = $result->fetch_assoc()) {
		$team_name= $row['team_name'];
		$team_code= $row['team_code'];
		$city= $row['city'];
		$province= $row['province'];
		$contact_name= $row['contact_name'];
		$email= $row['email'];
		$phone= $row['phone'];

		echo "<tr style=\"height: 1.0em; text-align:center;\">
				<td>$team_name</td>
				<td>$team_code</td>
				<td>$city</td>
				<td>$province</td>
				<td>$contact_name</td>
				<td>$email</td>
				<td>$phone</td>
				<td><a href=\"admin_team_edit.php?team_name=$team_name&team_code=$team_code&city=$city&province=$province&contact_name=$contact_name&email=$email&phone=$phone\"> <button class='btn btns edit-button' id='edit'>Edit</button></a></td>
			</tr>\n";
	}	
}

$conn->close();		
?>

	</tbody>
</table>

	<a href="admin_add_team.php"><button class="btn btns desktop-btn" id="add">Add New Team</button></a>
</div>

<script>
	/* add extra style sheet to deal with Safari's deficiencies */
	var ua = navigator.userAgent.toLowerCase(); 
	if (ua.indexOf('safari') != -1) { 
	  if (ua.indexOf('chrome') > -1) {
	  } else {
		var css = document.createElement('link');
		css.type = "text/css";
		css.rel = "stylesheet";
		css.href = "css/admin_6_Safari.css";

		var h = document.getElementsByTagName('head')[0];

		h.appendChild(css);
		
	  }
	}
</script>
<script>
	/* Code to adjust spacing because margin doesn't work properly in Safari */
	var ua = navigator.userAgent.toLowerCase(); 
	if (ua.indexOf('safari') != -1) { 
	  if (ua.indexOf('chrome') > -1) {
	  } else {
		var divX = document.createElement("div");
		divX.className = "div-x";
		var h = document.getElementsByTagName('table')[0];
	    h.appendChild(divX);
		var spaceDiv = document.getElementsByClassName('div-x');
		spaceDiv[0].style.height = "1.5em";
		var desktopTable = document.getElementsByClassName('desktop-table');
		desktopTable[0].style.border = "none";
		desktopTable[0].style.backgroundColor = "#f8f8f8";
	  }
	}
</script>
</body>