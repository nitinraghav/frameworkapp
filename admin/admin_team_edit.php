<?php

$pageTitle = "Edit Team";
$CSS= "admin_style.css";

include("../includes/admin_header.php");
include("../includes/dbc.php");

$team_name= $_GET['team_name'];
$team_code= $_GET['team_code'];
$city= $_GET['city'];
$province= $_GET['province'];
$contact_name= $_GET['contact_name'];
$email= $_GET['email'];
$phone= $_GET['phone'];

?>
    
<div class="container">
	<form class="desktop-form" action="admin_team_edit.php" method="POST">

		<label for="team_name" class="form-label">Team Name:</label>
		<input class="form-input" type="text" name="team_name" id="team_name" value=<?php echo $team_name?>><br>
		
		<label for="team_code" class="form-label">Team Code:</label>
		<input class="form-input" type="text" name="team_code" id="team_code" value=<?php echo $team_code?>><br>

		<label for="city" class="form-label">City: </label>
		<input class="form-input" type="text" name="city" id="city" value=<?php echo $city?>><br>
		
		<label for="province_pick" class="form-label">Province:</label>
		<select class="form-input" name="province_pick" id="admin_province_picker">
			<option selected= "selected"><?php echo $province?></option>
			<option value = "AB">AB</option>
			<option value = "BC">BC</option>
			<option value = "MB">MB</option>
			<option value = "NB">NB</option>
			<option value = "NL">NL</option>
			<option value = "NS">NS</option>
			<option value = "NT">NT</option>
			<option value = "NU">NU</option>
			<option value = "ON">ON</option>
			<option value = "PE">PE</option>
			<option value = "QC">QC</option>
			<option value = "SK">SK</option>
			<option value = "YT">YT</option>
		</select><br>	
		
		<label for="contact_name" class="form-label">Contact Name: </label>
		<input class="form-input" type="text" name="contact_name" id="contact_name" value=<?php echo $contact_name?>><br>
		
		<label for="emailid" class="form-label">Email: </label>
		<input class="form-input" type="email" name="email" id="emailid" value=<?php echo $email?>><br>
		
		<label for="phone" class="form-label">Phone Number: </label>
		<input class="form-input" type="number" name="phone" id="phone" value=<?php echo $phone?>><br>
		
		<div class="submit-btn-box">
		   <button type="submit" class="btn btns form-submit" id="add">Save Changes</button><br>
		</div>
		
	</form>
</div>	

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST'){

	$team_name= $_POST['team_name'];
	$team_code= $_POST['team_code'];
	$city= $_POST['city'];
	$province= $_POST['province_pick'];
	$contact_name= $_POST['contact_name'];
	$email= $_POST['email'];
	$phone= $_POST['phone'];

	$q= "UPDATE teams SET team_name='".$team_name."',team_code='".$team_code."',city='".$city."',province='".$province."',contact_name='".$contact_name."',email='".$email."',phone='".$phone."' WHERE team_code= '".$team_code."'";

	$r= mysqli_query($conn, $q);

	if($r){
?> 		<script> window.location.replace("admin_teams.php");</script> <?php
	} else {
		echo "<p> Error updating Team details</p>";
	}
}
?>
<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

<script>
	/* add extra style sheet to deal with Safari's deficiencies */
	var ua = navigator.userAgent.toLowerCase(); 
	if (ua.indexOf('safari') != -1) { 
	  if (ua.indexOf('chrome') > -1) {
	  } else {
		var css = document.createElement('link');
		css.type = "text/css";
		css.rel = "stylesheet";
		css.href = "css/admin_7_Safari.css";

		var h = document.getElementsByTagName('head')[0];
		h.appendChild(css);
	  }
	}
</script>
</body>