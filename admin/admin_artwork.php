<?php

$pageTitle = "Artwork/Prizes";
$CSS= "admin_style.css";
include("../includes/admin_header.php");

include("../includes/dbc.php");

$sql = 	"SELECT artwork.artwork_name, artwork.image_name, artwork.art_id, user.first_name, 
		user.last_name, user.city, user.province,  artwork.highest_bid_hours, user.user_id
		FROM artwork
		LEFT JOIN user
		ON artwork.winner_user_id=user.user_id";

$result = mysqli_query($conn, $sql);
$num= mysqli_num_rows($result);

if ($num!==0) {
	echo "<div class=\"container\">
			<table class=\"desktop-table\" style=\"text-align: center;\">
				<thead>
					<tr>
						<th>Artwork</th>
						<th>Code</th> 
						<th>Winner</th>
						<th>City</th>
						<th>Province</th>
						<th>% Completed</th>
					</tr>
				</thead>
				<tbody>";

	while($row = mysqli_fetch_assoc($result)) {
		$artwork_name= $row['artwork_name'];
		$image_name= $row['image_name'];
		$art_id=$row['art_id'];
		$first_name= $row['first_name'];
		$last_name= $row['last_name'];
		$city= $row['city'];
		$province= $row['province'];
		$highest_bid_hours= $row['highest_bid_hours'];
		$user_id= $row['user_id'];
			
		$query= "SELECT SUM(total_time) AS total_time FROM user_hour_log WHERE user_id = '".$user_id."' AND art_id='".$art_id."'";
		$result2= mysqli_query($conn, $query);
		$row2= $result2 -> fetch_assoc();
		$hours_logged= $row2['total_time'];

		if(!$highest_bid_hours===0){
			$percent_progress= number_format($hours_logged/$highest_bid_hours * 100 , 0);

		} else{
			$percent_progress= 0;
		}

		$file_path = 'http://framework.launchliveapp.com/webapp/images/';
		$src = $file_path.$image_name;

		echo "<tr style=\"height: 1.0em;\">
				<td style=\"text-transform: capitalize;\"><img src='$src' class=\"mini-pic\" style=\"float: left\"> $artwork_name</td>
				<td>$art_id</td>
				<td style=\"text-transform: capitalize;\">$first_name $last_name</td>
				<td style=\"text-transform: capitalize;\">$city</td>
				<td style=\"text-transform: capitalize;\">$province</td>
				<td>$percent_progress %</td>
			</tr>\n";
	}//end while
} else {
	echo "<h2> No results found</h2>";
}

$conn->close(); 
		
?>
		</tbody>
	</table>

	<div class="row">
		<div class="col-xs-4 col-sm-4">
			<a href="admin_add_artwork.php"><button class="btn btns multi-btn" id="addorg">Add New Artwork</button></a>
		</div>
		<div class="col-xs-4 col-sm-4">
			<button class="btn btns multi-btn" id="start_bid">Start Bidding</button>
		</div>
		<div class="col-xs-4 col-sm-4">
			<button class="btn btns multi-btn" id="stop_bid">Stop Bidding</button>
		</div>
	</div>
</div>
<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>
<script>
	$("#start_bid").on("click", function(e){
		e.preventDefault();
		$.post("start_stop_bid.php", {id: 1}, function(data){
			alert(data);
		});
	});

	$("#stop_bid").on("click", function(e){
		e.preventDefault();
		$.post("start_stop_bid.php", {id: 0}, function(data){
			alert(data);
		});
	});


</script>
<script>
	/* add extra style sheet to deal with Safari's deficiencies */
	var ua = navigator.userAgent.toLowerCase(); 
	if (ua.indexOf('safari') != -1) { 
	  if (ua.indexOf('chrome') > -1) {
	  } else {
		var css = document.createElement('link');
		css.type = "text/css";
		css.rel = "stylesheet";
		css.href = "css/admin_4_Safari.css";

		var h = document.getElementsByTagName('head')[0];

		h.appendChild(css);
		
	  }
	}
</script>

<script>
	/* Code to adjust spacing because margin doesn't work properly in Safari */
	var ua = navigator.userAgent.toLowerCase(); 
	if (ua.indexOf('safari') != -1) { 
	  if (ua.indexOf('chrome') > -1) {
	  } else {
		var br = document.createElement("br");
		var node = document.getElementsByClassName('row')[0];
		node.insertBefore(br,node.firstChild);
	  }
	}
</script>
</body>
</html>


