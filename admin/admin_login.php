<!doctype html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Log in</title>
	<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
	
	<link href="admin_style.css" rel="stylesheet" type="text/css">
	
</head>

<body class="desktop-body">
	

	<div class="strip-btn-box">
		<img src="../images/framework-whiteonclear.png" class="logo" alt="Framework Logo">
	</div><br><br><br>
	
    <div class="container-fluid">
		
		<div class="row centeralign">
			
			<form action="admin_login_backend.php" method="POST" id="login_form">
        		<input class="form-control desktop-input" name="email" id="email" type="email" placeholder="E-mail">

        		<input class="form-control desktop-input" name="password" id="password" type="password" placeholder="Password">

				<input type = "submit" class = "btn btns desktop-btn" id = "login" value="Log-in" name="login" />
			</form>

		</div>
	</div>
		
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

	<script>
     	$(document).ready(function(){

	        $('#login_form').on("submit", function(e) {
	          	e.preventDefault();

	        	var url = $(this).attr("action");
				var formData = $(this).serialize();
				//console.log(formData);

	          	$.ajax(url ,{
	            type: "POST",
	            data: formData,
	            
	            success: function (result) {
	              if (result=== "true"){
	              	window.location.replace("admin_reports.php");

	              } else if (result==="false") {
	              	alert("Incorrect Email or Password");
	              } 

	            }//end success
	          });//end ajax
	        });//end submit
      	});// end ready
    </script>
    <!--force https instead of http-->
	<script> if(location.protocol != "https:"){
	 	location.href = "https:" + window.location.href.substring(window.location.protocol.length);
	}
	</script>

</body>
</html>