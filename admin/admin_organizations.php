<?php

$pageTitle = "Organizations";
$CSS= "admin_style.css";
include("../includes/admin_header.php");

include("../includes/dbc.php");

$query= "SELECT * FROM organizations";
$result= mysqli_query($conn, $query);
$num= mysqli_num_rows($result);

if($num===0){
	echo "<p>There are no Organizations to display</p>";

} else {
	echo "<div class=\"container\">
			<table class=\"desktop-table\">
				<thead>
					<tr>
						<th>Organization Id</th>
						<th>Organization Name</th>
						<th>Charitable Id</th>
						<th>Contact Name</th>
						<th>Email</th>
						<th>Phone Number</th>
					</tr>
				</thead>
				<tbody>";

	// output data of each row
	while($row = $result->fetch_assoc()) {
		$organization_id= $row['organization_id'];
		$organization_name= $row['organization_name'];
		$charitable_id= $row['charitable_id'];
		$contact_name= $row['contact_name'];
		$email= $row['email'];
		$phone= $row['phone'];

	echo "<tr style=\"height: 1.0em; text-align:center;\">
			<td>$organization_id</td>
			<td>$organization_name</td>
			<td>$charitable_id</td>
			<td>$contact_name</td>
			<td>$email</td>
			<td>$phone</td>
			<td><a href=\"admin_organization_edit.php?organization_id=$organization_id&organization_name=$organization_name&charitable_id=$charitable_id&contact_name=$contact_name&email=$email&phone=$phone\"> <button class='btn btns edit-button' id='edit'>Edit</button></a></td>
		</tr>\n";
	}	
}

?>

	</tbody>
</table>
	<a href="admin_add_organization.php"><button class="btn btns desktop-btn" id="add">Add New Organization</button></a>
</div>

<script>
	/* add extra style sheet to deal with Safari's deficiencies */
	var ua = navigator.userAgent.toLowerCase(); 
	if (ua.indexOf('safari') != -1) { 
	  if (ua.indexOf('chrome') > -1) {
	  } else {
		var css = document.createElement('link');
		css.type = "text/css";
		css.rel = "stylesheet";
		css.href = "css/admin_6_Safari.css";
		var h = document.getElementsByTagName('head')[0];
		h.appendChild(css);	
	  }
	}
</script>
<script>
	/* Code to adjust spacing because margin doesn't work properly in Safari */
	var ua = navigator.userAgent.toLowerCase(); 
	if (ua.indexOf('safari') != -1) { 
	  if (ua.indexOf('chrome') > -1) {
	  } else {
		var divX = document.createElement("div");
		divX.className = "div-x";
		var h = document.getElementsByTagName('table')[0];
	    h.appendChild(divX);
		var spaceDiv = document.getElementsByClassName('div-x');
		spaceDiv[0].style.height = "1.5em";
		var desktopTable = document.getElementsByClassName('desktop-table');
		desktopTable[0].style.border = "none";
		desktopTable[0].style.backgroundColor = "#f8f8f8";	
	  }
	}
</script>
</body>