<?php
error_reporting(E_ALL);
$pageTitle = "Add New Artwork";
$CSS= "admin_style.css";

include("../includes/admin_header.php"); 
?>
    
<div class="container">
	<form class="desktop-form" action="admin_add_artwork.php" method="POST" enctype="multipart/form-data">
	
		<label for="artwork_name" class="form-label">
		Artwork Name: </label>
		<input class="form-input" type="text" name="artwork_name" id="artwork_name" size=255 required="required"><br>
		
		<label for="filep" class="form-label">
		Picture: </label>
		<input class="form-input" type="file" id="filep" name="filep" required="required"><br>
		
		<label for="artist_name" class="form-label">
		Artist: </label>
		<input class="form-input" type="text" name="artist_name" id="artist_name" size=255 required="required"><br>
		
		<div class="submit-btn-box">
		    <input type="submit" class="btn btns form-submit" id="addart" name="submit"><br>
		</div>

	</form>
</div>


<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	if (isset($_POST['artwork_name']) && isset($_POST['artist_name']) ){
		$artwork_name = $_POST['artwork_name'];
		$artist_name = $_POST['artist_name'];

		//$folder = "http://framework.launchliveapp.com/webapp/images/";
		getcwd();
		chdir('../images');
		
		$move = move_uploaded_file($_FILES["filep"]["tmp_name"] , $_FILES["filep"]["name"]);

		if($move){
			include("../includes/dbc.php");

			$query = "INSERT INTO artwork(art_id, artwork_name, image_name, artist_name, highest_bid_hours, winner_user_id) VALUES ('', '".$artwork_name."', '".$_FILES['filep']['name']."', '".$artist_name."', '', '')";

			$result = mysqli_query($conn, $query);
			
			if($result) { 
				?>
				<script> alert('File <?php echo $_FILES["filep"]["name"] ;?> successfully uploaded') ;
				</script> 
			<?php
				echo "<script>window.location='https://framework.launchliveapp.com/webapp/admin/admin_artwork.php'</script>";
			} else {
				//Gives and error if its not
				echo '<script> alert("Sorry, there was a problem uploading your file");</script>';
			}
		} 
	}
} 
?>
<!--image library -->
<script type="text/javascript" href="../croppic/croppic.js">
	var cropperHeader = new Croppic('filep');
	var cropperOptions = {
			uploadUrl: <?php echo $_FILES["filep"]["name"]; ?>
		}		
		
	var cropperHeader = new Croppic('filep', cropperOptions);
					
</script>
<script>
	var ua = navigator.userAgent.toLowerCase(); 
	if (ua.indexOf('safari') != -1) { 
	  if (ua.indexOf('chrome') > -1) {
	  } else {
		var css = document.createElement('link');
		css.type = "text/css";
		css.rel = "stylesheet";
		css.href = "css/admin_5_Safari.css";

		var h = document.getElementsByTagName('head')[0];

		h.appendChild(css);
	  }
	}
</script>

</body>
</html>