<?php

$pageTitle = "Add New Organization";
$CSS= "admin_style.css";
include("../includes/admin_header.php");
include("../includes/dbc.php");
?>

<div class="container">
	<form class="desktop-form" action="admin_add_organization.php" method="POST">
	
		<label for="organization_name" class="form-label">Organization Name:</label>
		<input class="form-input" type="text" name="organization_name" id="organization_name" required><br>
		
		<label for="charity_id" class="form-label">Charitable ID: </label>
		<input class="form-input" type="text" name="charitable_id" id="charity_id"><br>
		
		<label for="contact_name" class="form-label">Contact Name: </label>
		<input class="form-input" type="text" name="contact_name" id="contact_name"><br>
		
		<label for="emailid" class="form-label">Email: </label>
		<input class="form-input" type="email" name="email" id="emailid"><br>
		
		<label for="phone" class="form-label">Phone Number: </label>
		<input class="form-input" type="text" name="phone" id="phone"><br>

		<label for="city_id" class="form-label">City: </label>
		<input class="form-input" type="text" name="city" id="city"><br>

		<label for="prov_id" class="form-label">Province: </label>
		<select class="form-input" name="province" id="prov_id">
			<option value="AB">Alberta</option>
			<option value="BC">British Columbia</option>
			<option value="MB">Manitoba</option>
			<option value="NB">New Brunswick</option>
			<option value="NL">Newfoundland and Labrador</option>
			<option value="NS">Nova Scotia</option>
			<option value="ON">Ontario</option>
			<option value="PE">Prince Edward Island</option>
			<option value="QC">Quebec</option>
			<option value="SK">Saskatchewan</option>
			<option value="NT">Northwest Territories</option>
			<option value="NU">Nunavut</option>
			<option value="YT">Yukon</option>
		</select>
			
		<div class="submit-btn-box">	   
		   <button type="submit" class="btn btns form-submit" id="add">Add New Organzation/Save</button><br>
		</div>
		
	</form>
</div>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST'){

	$organization_name= $_POST['organization_name'];
	$charitable_id= $_POST['charitable_id'];
	$contact_name= $_POST['contact_name'];
	$email= $_POST['email'];
	$phone= $_POST['phone'];
	$city= $_POST['city'];
	$province= $_POST['province'];

	//error handling
	if(empty($organization_name)){
		echo "<p> Please enter an Organization name</p>";
	}
	if(empty($charitable_id)){
		echo "<p> Please enter a Charity id</p>";
	} 
	if(empty($contact_name)){
		echo "<p> Please enter a Contact name</p>";
	} 
	if(empty($email)){
		echo "<p> Please enter an Email id</p>";
	} 
	if(empty($phone)){
		echo "<p> Please enter a Phone number</p>";
	} 
	if(empty($city)){
		echo "<p> Please enter a City</p>";
	} 
	if(empty($province)){
		echo "<p> Please select a Province</p>";
	}

	$q= "INSERT INTO organizations(organization_id, organization_name, charitable_id, contact_name, email, phone, city, province) VALUES ('', '".$organization_name."', '".$charitable_id."', '".$contact_name."', '".$email."', '".$phone."', '".$city."', '".$province."')";

	$r= mysqli_query($conn, $q);

	if($r){
?> 		<script> window.location.replace("admin_organizations.php");</script> <?php
	} else {
		echo "<p> Error saving Team details</p>";
	}
}
?>
<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

<script>
	/* add extra style sheet to deal with Safari's deficiencies */
	var ua = navigator.userAgent.toLowerCase(); 
	if (ua.indexOf('safari') != -1) { 
	  if (ua.indexOf('chrome') > -1) {
	  } else {
		var css = document.createElement('link');
		css.type = "text/css";
		css.rel = "stylesheet";
		css.href = "css/admin_10_Safari.css";
		var h = document.getElementsByTagName('head')[0];
		h.appendChild(css);
		
	  }
	}
</script>
</body>