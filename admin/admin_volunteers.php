<?php 

$pageTitle = "Volunteers";
$CSS= "admin_style.css";
include("../includes/admin_header.php");
   
include("../includes/dbc.php");

$sql= "SELECT user.first_name, 
		user.last_name, user.email, user.city, user.province, artwork.artwork_name, artwork.highest_bid_hours, artwork.art_id, user.user_id
		FROM artwork
		RIGHT JOIN user
		ON artwork.winner_user_id = user.user_id";

$result = mysqli_query($conn, $sql);

$num= mysqli_num_rows($result);

if ($num!==0) {
	echo "<div class=\"container\">
			<table class=\"desktop-table\" style=\"text-align: center;\">
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th> 
						<th>City</th>
						<th>Province</th>
						<th>Artwork</th>
						<th>% Completed</th>
					</tr>
				</thead>
				<tbody>";

	// output data of each row
	while($row = $result->fetch_assoc()) {
		$first_name= $row['first_name'];
		$last_name= $row['last_name'];
		$email= $row['email'];
		$city= $row['city'];
		$province= $row['province'];
		$artwork_name= $row['artwork_name'];
		$highest_bid_hours= $row['highest_bid_hours'];
		$art_id= $row['art_id'];
		$user_id= $row['user_id'];

		$query2= "SELECT SUM(total_time) AS total_time FROM user_hour_log WHERE user_id = '".$user_id."' AND art_id='".$art_id."'";
		$result2= mysqli_query($conn, $query2);
		$row2= $result2 -> fetch_assoc();
		$hours_logged= $row2['total_time'];

		if(!$highest_bid_hours==0){
			$percent_progress= number_format($hours_logged/$highest_bid_hours * 100 , 0);

		} else{
			$percent_progress= 0;
		}

		echo "<tr style=\"height: 1.0em; text-align:center;\">
				<td style=\"text-transform: capitalize;\">$first_name $last_name</td>
				<td>$email</td>
				<td style=\"text-transform: capitalize;\">$city</td>
				<td style=\"text-transform: capitalize;\">$province</td>
				<td style=\"text-transform: capitalize;\">$artwork_name</td>
				<td>$percent_progress %</td>
			</tr>\n";
	}// end while
	echo "</tbody>
	</table>";
} else {
	echo "<h2> No results found</h2>";
}

$conn->close(); 
		
?>
		</tbody>
	</table>
</div>
<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

<script>
	/* add extra style sheet to deal with Safari's deficiencies */
	var ua = navigator.userAgent.toLowerCase(); 
	if (ua.indexOf('safari') != -1) { 
	  if (ua.indexOf('chrome') > -1) {
	  } else {
		var css = document.createElement('link');
		css.type = "text/css";
		css.rel = "stylesheet";
		css.href = "css/admin_3_Safari.css";

		var h = document.getElementsByTagName('head')[0];

		h.appendChild(css);
	  }
	}
</script>

</body>
</html>