<?php

if(!isset($_COOKIE['email'])){
	header("location: admin_login.php");
	exit;
}

$CSS= "admin_style.css";
$pageTitle = "Reports";
include("../includes/admin_header.php"); 

?>
<script>
/* add extra style sheet to deal with Safari's deficiencies */
var ua = navigator.userAgent.toLowerCase(); 
if (ua.indexOf('safari') != -1) { 
  if (ua.indexOf('chrome') > -1) {
  } else {
	var css = document.createElement('link');
	css.type = "text/css";
	css.rel = "stylesheet";
	css.href = "css/admin_2_Safari.css";

	var h = document.getElementsByTagName('head')[0];

	h.appendChild(css);
	
  }
}

</script>
	<div class="container">
		<div class="reports-output">
			Reports<br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Total number of volunteers, hours pledged, hours completed<br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Total number of Non-Profits<br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Total number of volunteers, hours pledged, completed by organization<br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Download to csv or xls- import to Salesforce<br>
		</div>
	</div>
</body>
</html>