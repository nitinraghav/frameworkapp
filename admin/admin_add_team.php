<?php

$pageTitle = "Add New Team";
$CSS= "admin_style.css";
include("../includes/admin_header.php");

include("../includes/dbc.php");
?>

<div class="container">
	<form class="desktop-form" action="admin_add_team.php" method="POST">
	
		<label for="team_name" class="form-label">Team Name:</label>
		<input class="form-input" type="text" name="team_name" id="team_name" required><br>

		<label for="team_name" class="form-label">Team Code:</label>
		<input class="form-input" type="text" name="team_code" id="team_code" required><br>
		
		<label for="city" class="form-label">City: </label>
		<input class="form-input" type="text" name="city" id="city"><br>

		<label for="province_pick" class="form-label">Province:</label>
		<select class="form-input" name="province_pick" id="admin_province_picker">
			<option value = "" hidden ></option>
			<option value = "AB">AB</option>
			<option value = "BC">BC</option>
			<option value = "MB">MB</option>
			<option value = "NB">NB</option>
			<option value = "NL">NL</option>
			<option value = "NS">NS</option>
			<option value = "NT">NT</option>
			<option value = "NU">NU</option>
			<option value = "ON">ON</option>
			<option value = "PE">PE</option>
			<option value = "QC">QC</option>
			<option value = "SK">SK</option>
			<option value = "YT">YT</option>
		</select><br>
		
		<label for="contact_name" class="form-label">Contact Name: </label>
		<input class="form-input" type="text" name="contact_name" id="contact_name"><br>
		
		<label for="emailid" class="form-label">Email: </label>
		<input class="form-input" type="email" name="email" id="emailid"><br>
		
		<label for="phone" class="form-label">Phone Number: </label>
		<input class="form-input" type="number" name="phone" id="phone"><br>
		
		<div class="submit-btn-box">
		   <button type="submit" class="btn btns form-submit" id="add">Add New Team</button><br>
		</div>
		
	</form>
	</div>		
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST'){

	$team_name= $_POST['team_name'];
	$team_code= $_POST['team_code'];
	$city= $_POST['city'];
	$province= $_POST['province_pick'];
	$contact_name= $_POST['contact_name'];
	$email= $_POST['email'];
	$phone= $_POST['phone']; 

	$q= "INSERT INTO teams(team_name, team_code, city, province, contact_name, email, phone) VALUES ('".$team_name."', '".$team_code."', '".$city."', '".$province."', '".$contact_name."', '".$email."', '".$phone."')";
	$r= mysqli_query($conn, $q);

	if($r){
?> 		<script> window.location.replace("admin_teams.php");</script> <?php
	} else {
		echo "<p> Error saving Team details</p>";
	}
}
?>
<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

<script>
	/* add extra style sheet to deal with Safari's deficiencies */
	var ua = navigator.userAgent.toLowerCase(); 
	if (ua.indexOf('safari') != -1) { 
	  if (ua.indexOf('chrome') > -1) {
	  } else {
		var css = document.createElement('link');
		css.type = "text/css";
		css.rel = "stylesheet";
		css.href = "css/admin_7_Safari.css";

		var h = document.getElementsByTagName('head')[0];

		h.appendChild(css);
	  }
	}
</script>
</body>