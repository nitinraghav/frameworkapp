<?php

$pageTitle = "Edit Organization";
$CSS= "admin_style.css";

include("../includes/admin_header.php");
include("../includes/dbc.php");

$organization_id= $_GET['organization_id'];
$organization_name= $_GET['organization_name'];
$charitable_id= $_GET['charitable_id'];
$contact_name= $_GET['contact_name'];
$email= $_GET['email'];
$phone= $_GET['phone'];

?>
<div class="container">
	
	<form class="desktop-form" action="admin_organization_edit.php" method="POST">

		<label id="team_id_label" for="organization_id">Organization ID:</label>
		<input type="number" name="organization_id" id="organization_id"
		value="<?php echo $organization_id;?>" readonly="readonly">
			
		<a href="https://framework.launchliveapp.com/webapp/admin/admin_organization_delete.php?id=<?php echo $organization_id; ?>"><img style="float:right; max-height: 50px; " src="../images/deletebutton.png" id="delete_img"></a><br>

		<label for="organization_name" class="form-label">Organization Name:</label>
		<input class="form-input" type="text" name="organization_name" id="organization_name" required 
		value=<?php echo $organization_name;?>><br>
		
		<label for="charity_id" class="form-label">Charitable ID: </label>
		<input class="form-input" type="text" name="charitable_id" id="charity_id" 
		value=<?php echo $charitable_id;?>><br>
		
		<label for="contact_name" class="form-label">Contact Name: </label>
		<input class="form-input" type="text" name="contact_name" id="contact_name" 
		value=<?php echo $contact_name;?>><br>
		
		<label for="emailid" class="form-label">Email: </label>
		<input class="form-input" type="email" name="email" id="emailid" 
		value=<?php echo $email;?>><br>
		
		<label for="phone" class="form-label">Phone Number: </label>
		<input class="form-input" type="text" name="phone" id="phone" 
		value=<?php echo $phone;?>><br>
		
		<div class="submit-btn-box">
		   <button type="submit" class="btn btns form-submit" id="edit_org">Save Changes</button><br>
		</div>
		
	</form>
</div>

<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST'){

	$organization_id=$_POST['organization_id'];
	$organization_name= $_POST['organization_name'];
	$charitable_id= $_POST['charitable_id'];
	$contact_name= $_POST['contact_name'];
	$email= $_POST['email'];
	$phone= $_POST['phone'];

	$query= "UPDATE organizations SET organization_name='".$organization_name."',charitable_id='".$charitable_id."', contact_name='".$contact_name."', email='".$email."',phone='".$phone."' WHERE organization_id= '".$organization_id."'";

	$result= mysqli_query($conn, $query);
	

	if($result){
		echo '<script> window.location.replace("admin_organizations.php");</script>';
	} else {
		echo "<p>Error updating Organization details</p>";
	}
}

?>
<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

<script type="text/javascript">
	$("#delete_img").on("click", function(e){
		e.preventDefault();

		alert("Are you sure you want to delete this Organization?");
		window.location.replace("https://framework.launchliveapp.com/webapp/admin/admin_organization_delete.php?id=<?php echo $organization_id; ?>");
	});
</script>

<script>
	/* add extra style sheet to deal with Safari's deficiencies */
	var ua = navigator.userAgent.toLowerCase(); 
	if (ua.indexOf('safari') != -1) { 
	  if (ua.indexOf('chrome') > -1) {
	  } else {
		var css = document.createElement('link');
		css.type = "text/css";
		css.rel = "stylesheet";
		css.href = "css/admin_11_Safari.css";
		var h = document.getElementsByTagName('head')[0];
		h.appendChild(css);	
	  }
	}
</script>
<script>
	/* Code to adjust spacing because margin doesn't work properly in Safari */
	var ua = navigator.userAgent.toLowerCase(); 
	if (ua.indexOf('safari') != -1) { 
	  if (ua.indexOf('chrome') > -1) {
	  } else {
		var space = document.querySelector("form[action]");
		space.style.marginBottom = "-20px";
	  }
	}
</script>
</body>