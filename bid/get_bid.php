<?php 
session_start();

//check if user is logged in
if(!isset($_SESSION['user_id'])){
	header("location: ../login.php");
} else {
	//caching the user id from set cookie
	$user_id= $_SESSION['user_id'];
	
	//connect to DB
	include("../includes/dbc.php");

	//--------------display artwork-------------------------

	//check if artwork id entered
	if(isset($_POST['artwork_id'])){
		//caching artwork id 
		$artwork_id= $_POST['artwork_id'];

		//artwork id stored in session global to use in update_bid.php
		$_SESSION['artwork_id'] = $artwork_id;
		
		//check if artwork exists
 		$query= "SELECT * FROM artwork WHERE art_id = '".$artwork_id."'";
		$result = mysqli_query($conn, $query);
		$num= mysqli_num_rows($result);

		if ($num ==0)  {
			echo "<h3>Artwork id does not exist</h3><br /> <br />";

		} else {
			$row = mysqli_fetch_assoc($result);
			//getting info from table row for particular art id 
			$winner_user_id = $row['winner_user_id'];
			$artwork_name = $row['artwork_name'];
			$image_name = $row['image_name'];

			//if there is a winner for that artwork display winner details
			if ($winner_user_id){
				//getting name of the winner 
				$q= "SELECT * FROM user WHERE user_id= ".$winner_user_id."";
				$r = mysqli_query($conn, $q);
				$row3 = mysqli_fetch_assoc($r);
				$first_name = $row3['first_name'];
				$last_name = $row3['last_name'];

				echo "<h3 id='winner_text'> Winner: ".$first_name." ".$last_name."</h3>";
			} 
			echo "<br/><h3 id = 'artwork_name'>".$artwork_name."</h3>";

			//setting source of image from image folder
			$file_path = 'http://framework.launchliveapp.com/webapp/images/';
			$src = $file_path.$image_name;

			//display image
			echo '<img src="'.$src.'" alt="No Image Available" id= "artwork_pic" class= "img-responsive">';

			//query to DB(bids table)
			$query2 = "SELECT MAX(bid_amount_hours) AS bid_amount_hours FROM bids WHERE art_id = '".$artwork_id."'";
			$result2 = mysqli_query($conn, $query2);
			$row2 = mysqli_fetch_assoc($result2);
			$bid_amount_hours = $row2['bid_amount_hours'];

			//saving bid_amount_hours to be used in update_bid.php
			$_SESSION['bid_amount_hours'] = $bid_amount_hours;

			//display Current bid
			echo '<label id="label_current_bid" style: "float: left">Current Bid</label>
				  <input class="form-control" type="text" name="current_bid" value="'.$bid_amount_hours.'" id ="current_bid" readonly/> <br/>';

			//check if bidding is started or stopped
			$sql= "SELECT * FROM settings";
			$result4 = mysqli_query($conn, $sql);
			$row4= $result4-> fetch_assoc();
			$bid_status= $row4['bidding'];
			
			//if bidding is ON and no winner is assigned then allow bidding
			if($bid_status==1 && empty($winner_user_id)){
				
				if ($bid_amount_hours <=140){
					$next_bid= $bid_amount_hours+10;

					echo '<form action="bid/update_bid.php" method="POST" id="bid">
						<p id="next_bid"> Next Bid </p>
						<input class="form-control" type="text" name ="next_bid" id="new_bid" value="'.$next_bid.'" readonly>
						<button type="submit" class="btn btn-block btn-primary" id ="bid_now" name="bid_now" >Bid Now </button><br/>
						</form>
						<div id="response2"></div>';

				} else {
					echo "<h3> This Artwork already has a Winner</h3><br />";
				}
			} else if(!empty($winner_user_id)) {
				echo '<h3>This artwork already has a winner</h3>';
			} else if($bid_status==0){
				echo '<h3>Bidding is currently not allowed</h3>';
			}

		} 

	} else {
		echo "<h3>Please enter Artwork id</h3> <br /> <br />";
	}
}

