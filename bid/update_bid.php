<?php
session_start();
//caching variables from get_artwork.php
$artwork_id = $_SESSION['artwork_id'];
$bid_amount_hours = $_SESSION['bid_amount_hours'];

//check if user is logged in
if(!isset($_SESSION['user_id'])){
	header("location: ../login.php");
} else {
	//caching the user id from set cookie
	$user_id= $_SESSION['user_id'];

	//connect to DB
	include("../includes/dbc.php");

	//------------------------display bid hours---------------------------
	if(isset($_POST['next_bid'])) {

		//caching next_bid from user input value in get_artwork.php
		$next_bid = $_POST['next_bid'];

		if ($artwork_id == null ) {
			echo "invalidbid";
		} else {
			//check if new bid is greater than existing bid and less/equal to 140
			if($next_bid <= 140 && $next_bid== $bid_amount_hours+10  )  {
				$qry = "INSERT INTO bids(user_id, bid_id, bid_amount_hours, art_id,date_time_stamp) VALUES ('".$user_id."', '', '".$next_bid."','".$artwork_id."', NOW())";
				$result2= mysqli_query($conn, $qry);

				if(!$result2){
					echo "errorupdating";
				} else {
					echo "bidupdated";
				}	
			
			//check if new bid is greater than existing bid and equal to 150
			} else if ($next_bid == 150 && $next_bid== $bid_amount_hours+10) {
				$qry = "INSERT INTO bids(user_id, bid_id,bid_amount_hours, art_id,date_time_stamp) VALUES ('".$user_id."', '', '".$next_bid."','".$artwork_id."', NOW())";
				$result2= mysqli_query($conn, $qry);
				//if bid updated or Inserted in BIDS table update winner
				$q = "UPDATE artwork SET winner_user_id='".$user_id."' WHERE art_id='".$artwork_id."'";
				$r = mysqli_query($conn, $q);

				if($result2 && $r){
					echo "bidupdated";
				} else {
					echo "errorupdating";
				}
			} else {
				echo "invalidbid";
			}
		} 

	} else {
		echo "enterbid";
	}
}

