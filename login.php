<?php 
error_reporting(E_ERROR | E_PARSE);

//force https instead of http
echo '<script>if (location.protocol != \'https:\')
{
 location.href = \'https:\' + window.location.href.substring(window.location.protocol.length);
}</script>';

//converting http to https requests
//if($_SERVER["HTTPS"] != "on")
//{
 //      header("location:https://framework.launchliveapp.com/webapp/login.php");
 //   }

session_start();
$pageTitle = "Login | Framework";
$pageHeader = "Login";
include("includes/header.php");

//$_SESSION['user_logged_in']= 0;
require_once __DIR__ . '/src/Facebook/autoload.php';

$fb = new Facebook\Facebook([
  'app_id' => '1871941856406133',                   			// App id
  'app_secret' => '28a8e84bcb9601b2a496e986c8a3333f',		// App secret
  'default_graph_version' => 'v2.9',
  ]);

$helper = $fb->getRedirectLoginHelper();

$permissions = ['email']; 

$loginUrl = $helper->getLoginUrl('https://framework.launchliveapp.com/webapp/fb-backend.php', $permissions);

?>
	<div class = "login_screen">
		<div class="row" id = "content">

			<div class="col-md-12" id ="lang_selector">
				<span id="eng">Eng</span> | <span id="french"> Fr </span>
			</div>

			<div class="col-md-12">
				<a href=<?php echo $loginUrl; ?>><img class="img-responsive fb-login-img" src="images/fblogin.png"></a>
			</div>

			<div class="col-md-12 txt">Or use your email:</div>

			<div class="col-md-12">
				<form action="login_validation.php" method="POST" id="form">
					<input class="form-control" type="email" name ="user" placeholder="email" id="email" required>
	       			<input class="form-control" type="password" name="pass" placeholder="password" id="password" required> <div class="eror"></div>
	       			
	      			<button type="submit" class="btn btn-primary buttons" name="login" value="" id ="" >Login </button>
				</form>
			</div>
			<div class="col-md-12">
				<a href="password_forgot.php"><button type="submit" class="btn btn-primary buttons" id ="forgot_password_btn" >Forgot Password</button></a>

				<a href="sign_up/sign_up.php"><button type="submit" class="btn btn-primary buttons" id ="" >Sign-up</button></a> <br />
			</div>
		</div>		
		
	</div>
<!--end container -->

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

    <script>
     	$(document).ready(function(){
     		//login button
	        $('#form').submit(function(e) {
	          	e.preventDefault();

	        	var url = $(this).attr("action");
				var formData = $(this).serialize();

	          	$.ajax(url ,{
	            type: "POST",
	            data: formData,
	            
	            success: function (result) {
	              if (result=== "true"){
	              	window.location.replace("index.php");

	              } else if (result==="false") {
	              	alert("Incorrect Email or Password");
	              } 

	            }//end success
	          });//end ajax
	        });//end submit
      	});// end ready
    </script>

    <script>
     	$(document).ready(function(){
     		//forgot password button
	        $("#forgot_password_btn").on("click", function(e) {
	        	e.preventDefault();

	        	$(".container").html('<form action="password_forgot.php" method="POST" id="forgot_password_form"><label id="forgot_password_text">Enter Your Email Id:</label><input type="text" name="email" class="form-control" required="required"/> <input class="btn btn-block btn-primary" type="submit" value="Reset My Password" id="forgot_password_button"/></form>');
	        });//end click

	        $(".container").on("submit", "#forgot_password_form", function(evt){
	        	evt.preventDefault();

	        	var url= $(this).attr("action");
	        	var formData= $(this).serialize();

	        	$.post(url, formData, function(data){

	        		if(data=="emailnotentered"){
	        			alert("Please enter your email id");

	        		} else if(data=='invalidemail'){
	        			alert("Please enter a valid email address");

	        		} else if (data=="emailnotavailable"){
	        			alert("Email id does not exist");
	        			
	        		} else {
	        			alert(data);
	        		}

	        	});//end post
	        });//end submit
	    });//end ready
	</script>
</body>
</html>