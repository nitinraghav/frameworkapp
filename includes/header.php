<!DOCTYPE html>
<html >

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title><?php echo $pageTitle; ?></title>

  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
  
  <link rel="stylesheet" type="text/css" href="https://framework.launchliveapp.com/webapp/style.css"  >

</head>

<body>
	<div class="container">

		<!-- navbar starts -->
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			
			<div class="container-fluid">
			
			  <div class="collapse navbar-collapse" id="myNavbar">
			    <ul class="nav navbar-nav ">
			      <li><a href="https://framework.launchliveapp.com/webapp/index.php">Home</a></li>
			      <li><a href="https://framework.launchliveapp.com/webapp/about.php">About</a></li>
			      <?php 
			      if(isset($_SESSION['facebook_access_token'])){
			      	echo '<li><a href="https://framework.launchliveapp.com/webapp/sign_out_fb.php">Sign-out</a></li>';
			      } else {
			        echo '<li><a href="https://framework.launchliveapp.com/webapp/sign_out.php">Sign-out</a></li>	';
			      } ?>
			    </ul>
			  </div>

			  <div class="navbar-header">
			    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar" aria-expanded="false">
			      <span class="sr-only">Toggle navigation</span>
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span> 
			    </button> 
			
			   <div class="navbar-brand-box"> 
			   <a class="navbar-brand" href="#"><? echo $pageHeader; ?></a>
			  </div>

			    <span><img src="https://framework.launchliveapp.com/webapp/images/logo.png" id="logo"></span> 
               </div>
			  

			</div>
		</nav>
		<!-- navbar ends -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>