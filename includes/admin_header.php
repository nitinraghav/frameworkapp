<!doctype html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $pageTitle ?></title>

	<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
	
	<link rel="stylesheet" type="text/css" href=<?php echo $CSS; ?> >
</head>

<!--force https instead of http-->
<script> if(location.protocol != "https:"){
 	location.href = "https:" + window.location.href.substring(window.location.protocol.length);
}
</script>

<script>
	/* add extra style sheet to deal with Safari's deficiencies */
	var ua = navigator.userAgent.toLowerCase(); 
	if (ua.indexOf('safari') != -1) { 
	  if (ua.indexOf('chrome') > -1) {
	  } else {
		var css = document.createElement('link');
		css.type = "text/css";
		css.rel = "stylesheet";
		css.href = "css/admin_header_Safari.css";

		var h = document.getElementsByTagName('head')[0];

		h.appendChild(css);
		
	  }
	}
</script>

<body class="desktop-body">

	<div class="strip-btn-box">
		<img src="../images/framework-whiteonclear.png" class="logo mini-logo" alt="Framework Logo">
		<span class="pull-right">
			<a href="admin_reports.php"><button class=" btn btns strip-btn" id ="reports">Reports</button></a>
			<a href="admin_volunteers.php"><button class=" btn btns strip-btn" id ="volunteers">Volunteers</button></a>
			<a href="admin_artwork.php"><button class=" btn btns strip-btn" id ="artwork">Artwork/Prizes</button></a>
			<a href="admin_teams.php"><button class=" btn btns strip-btn" id ="organizations">Teams</button></a>
			<a href="admin_organizations.php"><button class=" btn btns strip-btn" id ="organizations">Organizations</button></a>
			<a href="admin_login.php"><button class=" btn btns strip-btn" id ="logout">Log-out</button></a>
		</span>
	</div><br>