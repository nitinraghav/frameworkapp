<?php 
error_reporting(E_ALL);
session_start();
//check if user is logged in
if(!isset($_SESSION['user_id'])){
	header("location: ../login.php");

} else {
	//caching the user id from set cookie
	$user_id= $_SESSION['user_id'];
	
	//connect to DB
	include("../includes/dbc.php");

	//query DB for total hours for a user
	$query = "SELECT * FROM artwork WHERE winner_user_id = '".$user_id."'";
	$result = mysqli_query($conn, $query);
	$num_rows= mysqli_num_rows($result);

	if ($num_rows == 0){
		echo "noartwork";
		exit;

	} else{
		$art = [];
		//find multiple art ids assigned to a user
		//and store it in art[]
		while ($row = mysqli_fetch_assoc($result)) {
			$art[] =  $row['art_id'];
		}


		for ($i=0; $i<count($art); $i++){
		
			$query2= "SELECT *, SUM(total_time) AS total_time FROM user_hour_log WHERE user_id = '".$user_id."' && art_id = '".$art[$i]."'";
			$result2 = mysqli_query($conn, $query2);
			$row2 = mysqli_fetch_assoc($result2);
			$hours_completed_artwork = $row2['total_time'];		

			$query3= "SELECT * FROM artwork WHERE winner_user_id = '".$user_id."' && art_id = '".$art[$i]."'";
			$result3 = mysqli_query($conn, $query3);
			$row3 = mysqli_fetch_assoc($result3);
			$highest_bid_hours = $row3['highest_bid_hours'];
	
			$artwork_name= $row3['artwork_name'];
			$image_name = $row3['image_name'];

			//calculating percent completed
			if(!$highest_bid_hours==0){
				$percent= number_format($hours_completed_artwork/ $highest_bid_hours *100,0);
			} else {
				$percent= 0;
			}
			
			//setting source of image from image folder
			$file_path = 'https://framework.launchliveapp.com/webapp/images/';
			$src = $file_path.$image_name;

			if($percent<100) {	

				$html = '<div class="row box1" id="incomplete_art">\'+
			         \'<div class="col-xs-4 col-sm-4 col-md-4">\'+
			         \'<a href="artwork/prize.php?image_name='.$image_name.'&percent='.$percent.'&highest_bid_hours='.$highest_bid_hours.'&hours_completed_artwork='.$hours_completed_artwork.'" class="prize_img"><img class="img-responsive thumbnail" src='.$src.'></a>\'+
			         \'</div>\'+
			         \'<div class="col-xs-8 col-sm-8 col-md-8">\'+
			         \'<h6 ><span id="incomplete_art_name">'.$artwork_name.'</span><span id="percentage">'.$percent.'%</span> </h6>\'+
			         \'</div>\'+
			         \'</div>';

				$script_call = '<script type="text/javascript">
						$(document).ready(function() {
						    $("#on_going_art").append(\''.$html.'\');
						});
						</script>';
				echo $script_call;

			} else {
				$percent_array[]= $percent;

				$html= '<div class="row box1">\'+
			         	\'<div class="col-xs-4 col-sm-4 col-md-4">\'+
			         		\'<a href="artwork/prize.php?image_name='.$image_name.'&percent='.$percent.'" class="prize_img"><img class="img-responsive thumbnail" src='.$src.'></a>\'+
			         	\'</div>\'+
			         	\'<div class="col-xs-8 col-sm-8 col-md-8">\'+
			         		\'<h6>'.$artwork_name.'<span id="percentage">'.$percent.'%</span> </h6>\'+
			         	\'</div>\'+
			        \'</div>';

				$script_call = '<script type="text/javascript">
						$(document).ready(function() {
						    $("#completed_art").append(\''.$html.'\');
						});
						</script>';
				echo $script_call;    
			}//end if statement
		}//end for loop


		//ongoing artwork heading
		echo '<div class="row">
				<div class="row headings" >
					<div class=" col-md-12" >
			 			<h5 style="float:left"> On Going</h5>
			 		</div>	
				</div>
				<div id="on_going_art"></div>
			</div>';
		//display completed heading only if theres any completed artwork
		if(!empty($percent_array)){
			echo '<div class="row">
				<div class="row headings">
					<div class=" col-md-12" >
 						<h5 style="float:left"> Completed</h5>
 					</div>
				</div>
				<div id="completed_art"></div>
			</div>';
		}
	}
};

?>

</div> <!--end container-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
</body>
</html>