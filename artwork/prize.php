<?php 
session_start();
$image_name = $_GET['image_name'];
$percent = $_GET['percent'];
$hours_completed_artwork = $_GET['hours_completed_artwork'];
$highest_bid_hours = $_GET['highest_bid_hours'];

//check if user is logged in
if(!isset($_SESSION['user_id'])){
	header("location: ../login.php");

} else {
	//caching the user id from set cookie
	$user_id= $_SESSION['user_id'];
	
	//check if artwork name is set
	if (!isset($image_name)){
		header('location: ../index.php');

	} else{
		//connect to DB
		include("../includes/dbc.php");

		//query DB for total hours for a user
		$query = "SELECT image_name FROM artwork WHERE image_name = '".$image_name."'";
		$result = mysqli_query($conn, $query);

		if (mysqli_num_rows($result) == 0){
			echo "noimage";
			exit;

		} else{

			//setting source of image from image folder
			$file_path = 'https://framework.launchliveapp.com/webapp/images/';
			$src = $file_path.$image_name;
			
			echo '<div class="prize_container" id="prize_full_img" style="background: url('.$src.') no-repeat fixed center;" >
			
				<div class="row">
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
				</div>

				<div class="row">
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
				</div>
				<div class="row">
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
				</div>
				<div class="row">
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
				</div>
				<div class="row">
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
				</div>
				<div class="row">
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
				</div>
				<div class="row">
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
				</div>
				<div class="row">
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 box"></div>
				</div>

			</div>';

			$script_call = '<script type="text/javascript">
								$(document).ready(function() {
								    var randomElements = $(".box").get().sort(function() {
							       					return Math.round(Math.random()) - 0.5;
							    			}).slice(0, \''.$percent.'\');
								    $(randomElements).css(\'opacity\', \'0.3\');
								});
							</script>';
			echo $script_call;   
			if(!$hours_completed_artwork==0){
				echo "<br /><p>".$hours_completed_artwork." of ".$highest_bid_hours." volunteer hours logged</p>";
			} else{
				echo "<br /><p> 0 of ".$highest_bid_hours." volunteer hours logged</p>";
			}
		}
	}//end isset artwork_name
}// end cookie user_id
?>

</div> <!--end container-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
</body>
</html>