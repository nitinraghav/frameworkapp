<?php 
error_reporting(E_ERROR | E_PARSE);
$pageTitle = "Sign-up | Framework";
$pageHeader = "Sign-up";
include("../includes/header.php");

//converting http to https requests
if($_SERVER["HTTPS"] != "on") {
    $pageURL = "Location: https://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    header($pageURL);
}
?>
		<div id="form_container">
			<form action="sign_up_backend.php" method="POST" id="sign_up_form">
	       		<input class="form-control sing_up_form" type="text" placeholder ="First Name" name="first_name" required> 
	        	<input class="form-control sing_up_form" type="text" value="" placeholder="Last Name" name="last_name" required> 

	           	<input class="form-control sing_up_form" type="email" value="" placeholder="email" name="email" required> 

	           	<input class="form-control sing_up_form" type="password" value="" placeholder="password" name="password" id ="password" required> <span id="response1"></span>
		        <input class="form-control sing_up_form" type="password" value="" placeholder="Re-enter password" name="re_password" id="re_password" required> <span id="response2"></span>

				<h4 id ="optional">Optional:</h4> 
					
	  			<input class="form-control sing_up_form" type="text" value="" placeholder="Age" name="age"> 
	           	
	           	<select class="form-control" name="gender" id= "gender">
					<option hidden >Gender</option>
					<option>Male</option>
					<option>Female</option>
					<option>Decline to disclose</option>
				</select> 
				
				<input class="form-control sing_up_form" type="text" value="" placeholder="City" name="city"> 
				
				<select class="form-control" name="province" id="province">
					<option hidden >Province</option>
					<option>AB</option>
					<option>BC</option>
					<option>MB</option>
					<option>NB</option>
					<option>NL</option>
					<option>NS</option>
					<option>NT</option>
					<option>NU</option>
					<option>ON</option>
					<option>PE</option>
					<option>QC</option>
					<option>SK</option>
					<option>YT</option>
				</select> 

				<input class="form-control sing_up_form" type="text" value="" placeholder="Phone #" name="phone"> 
				<input class="form-control sing_up_form" type="text" value="" placeholder="Team Code" name= "team-code"> 
				<input class="form-control sing_up_form" type="text" value="" placeholder="12 Month Goal- Volunteer hours" name="volunteer-hours">
	       		
				<button type="submit" class="btn btn-primary" id ="sign_up_btn">Sign-up</button><br />
			</form>
			
		</div>
		
	</div> <!-- end container -->

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>
	
	<script>
		$(document).ready(function () {

		//client-side password restrictions atleast 6 characters long
			$("#password").on("keyup", function(){

				if($("#password").val().length < 6){
					$("#response1").html("Password should be atleast 6 characters long");
					
				} else if($("#password").val().length >= 6){
					$("#response1").html("");
				}
			});//end password length

		// client-side check if both passwords match
		    $("#re_password").on("keyup", function () {	
		        var matched,
		            password = $("#password").val(),
		            confirm = $("#re_password").val();
		        matched = (password === confirm) ? true : false;

		        if(matched) { 
		            //Shows success message and prevents submission.  In production, comment out the next 2 lines.
		            $("#response2").html("Passwords Match !");
		            $("#response2").css("color", "green");
			    }
			    else { 
			        $("#response2").html("Passwords don't match..");    
			        $("#response2").css("color", "red");
			    }
			});//end password match
		});//end ready
	</script>
	<script>
		$(document).ready(function() {

		//ajax call to add details in DB
			$('#sign_up_form').on("submit", function(e) {
	          	e.preventDefault();

	        	var url = $(this).attr("action");
				var formData = $(this).serialize();
				//console.log(formData);

	            $.ajax(url ,{
		            type: "POST",
		            data: formData,

		            success: function(data) {
		            	switch(data){
		            		case "emailexists":
			            		alert("Email id already exists \nPlease try Forgot Password");
			            		break;

			            	case "userupdated":
			            		window.location.replace("terms.php");
			            		break;

			            	case "dberror":
			            		$("#form_container").html("Database connection error try again later");
			            		break;

			            	default:
			            		window.location.replace("sign_up.php");
			            		break;
		            	}
	          		}
	          	})
	        });
		});//end ready
	</script>
</body>
</html>
