<?php
error_reporting(E_ERROR | E_PARSE);

$CSS = "../style.css";
$pageTitle = "Terms &amp; Conditions | Framework";
$pageHeader = "Terms";

include("../includes/header.php");

//converting http to https requests
if($_SERVER["HTTPS"] != "on") {
    $pageURL = "Location: https://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    header($pageURL);
}

// grab recaptcha library
require_once "recaptchalib.php";

if(isset($_POST['checkbox'])){
	//make connection to DB
	include("../includes/dbc.php");

	//secret key for google captcha
	$secret = "6LcvuhgUAAAAAMQ8-9Qr7l26QJqj5DmUuYD_EMMa";
	// empty response
	$response = null; 
	// check secret key
	$reCaptcha = new ReCaptcha($secret);
	// if submitted check response
	if ($_POST["g-recaptcha-response"]) {
	    $response = $reCaptcha->verifyResponse(
	        $_SERVER["REMOTE_ADDR"],
	        $_POST["g-recaptcha-response"]
	    );
	}
	
	//if captcha is clicked and response is success
	if ($response != null && $response->success) {
		session_start();
		$email_id= $_SESSION['email'];

		//updating database(user  table)
		$qry = "UPDATE user SET tnc_accepted = 'yes', date_time = NOW() WHERE email = '".$email_id."'";
		$result = mysqli_query($conn, $qry);

		if ($result){
			$sql = "SELECT * FROM user WHERE email='".$email_id."'";

			$result1 = mysqli_query($conn, $sql);
			$row = $result1 -> fetch_assoc();
			$user_id = $row['user_id'];

			
			?> <script> window.location.replace("../login.php");
			alert("Please login using your new account details"); </script><?php
		} else {
			?> <script> window.location.replace("sign_up.php");</script><?php
		}
	}
}

?>

<div class="row">

	<textarea class="form-control" id ="text_area" rows="15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus in lacus tincidunt, mattis massa a, tempor tellus. Proin viverra condimentum velit, a ultricies massa mattis in. Sed ipsum risus, gravida in dapibus in, ornare in neque. Etiam gravida nisi leo, sit amet laoreet velit lobortis quis. Nam nec consectetur ligula, in blandit magna. Donec commodo euismod suscipit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut ultricies purus nec enim bibendum sagittis. Etiam varius ex tortor, vel iaculis nulla volutpat in. Phasellus enim magna, cursus at rhoncus sed, mattis vel eros. Ut lobortis quam sed rutrum commodo. Mauris eget tortor in leo efficitur scelerisque id eu eros.
	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus in lacus tincidunt, mattis massa a, tempor tellus. Proin viverra condimentum velit, a ultricies massa mattis in. Sed ipsum risus, gravida in dapibus in, ornare in neque. Etiam gravida nisi leo, sit amet laoreet velit lobortis quis. Nam nec consectetur ligula, in blandit magna. Donec commodo euismod suscipit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut ultricies purus nec enim bibendum sagittis. Etiam varius ex tortor, vel iaculis nulla volutpat in. Phasellus enim magna, cursus at rhoncus sed, mattis vel eros. Ut lobortis quam sed rutrum commodo. Mauris eget tortor in leo efficitur scelerisque id eu eros</textarea>
	<br />
	
	
	<form action="terms.php" method="POST">
		<input type="checkbox" name="checkbox" id="checkbox" required> I agree to the terms of service <br />

		<!-- Recaptcha-->
		<div class="g-recaptcha" data-sitekey="6LcvuhgUAAAAANScUpQiKAO08cS68Y32qS6cdDvb"></div>

		<button type="submit" class="btn btn-primary" id ="terms_btn">Sign-up</button>
	</form>
	</div> <!-- end row -->
</div> <!-- end container -->

<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

<!-- Google recaptcha API-->
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src='https://www.google.com/recaptcha/api.js?hl=eng'></script>

</body>
</html>