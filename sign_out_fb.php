<?php
// Include FB config file
require_once 'fbConfig.php';

session_start();
// Remove access token from session

$url = 'https://www.facebook.com/logout.php?next=https://framework.launchliveapp.com/webapp/login.php' .
  '&access_token='.$accessToken;

unset($_SESSION['facebook_access_token']);

// Remove user data from session
unset($_SESSION['userData']);
unset($_SESSION['user_id']);
$_SESSION['user_logged_in']= null;
//setcookie('user_id', $user_id, time()-(60*60*24), "");

session_unset();
session_destroy();


if(empty($_SESSION)){

header('Location: '.$url);
}